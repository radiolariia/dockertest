Pull an image from Docker Hub

docker pull radiolariia/dockertest

Docker run app container

docker run --rm -it -p 8080:3000 radiolariia/dockertest

Docker compose

docker-compose up -d

Open http://localhost:8080/
